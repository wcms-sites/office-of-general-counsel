core = 7.x
api = 2

; uw_e_vote
projects[uw_e_vote][type] = "module"
projects[uw_e_vote][download][type] = "git"
projects[uw_e_vote][download][url] = "https://git.uwaterloo.ca/wcms/uw_e_vote.git"
projects[uw_e_vote][download][tag] = "7.x-1.1.1"
projects[uw_e_vote][subdir] = ""

